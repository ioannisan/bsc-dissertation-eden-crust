import {AuthenticationService} from "../_services/authentication.service";


export function appInitializer(authenticationService: AuthenticationService) /*() => Promise<unknown> */{

    return () => authenticationService.refreshToken()
      .subscribe(user =>
      console.log(user));

}
