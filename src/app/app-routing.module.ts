import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from "@angular/router";
import { WelcomePageComponent } from "./_components/_index/welcome-page.component";
import {AuthGuard} from "./_tools/auth.guard";
import {HomeComponent} from "./_components/home/home.component";
import {ProfileComponent} from "./_components/_profile/profile.component";
import {ProfileResolver} from "./_resolvers/profile.resolver";
import {UserSettingsComponent} from "./_components/settings/user-settings.component";
import {SettingsResolver} from "./_resolvers/settings.resolver";
import {NewComponent} from "./_components/_news/new.component";

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate:[AuthGuard] },
  { path: '', component: WelcomePageComponent },
  { path: 'login', component: WelcomePageComponent },
  { path: 'register', component: WelcomePageComponent},
  {
    path: 'profile/:username',
    component: ProfileComponent,
    canActivate: [AuthGuard],
    resolve: { user: ProfileResolver }},
  { path: 'settings/:username',
    component: UserSettingsComponent,
    canActivate: [AuthGuard],
    resolve: { user: SettingsResolver }},
  { path: 'related',
    component: NewComponent,
    canActivate: [AuthGuard]
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'welcome' }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule, RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
