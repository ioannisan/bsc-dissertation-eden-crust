import {Component, NgZone, OnInit} from '@angular/core';
import {PostService} from "../../../_services/post.service";
import {Post} from "../../../_models/post";
import {UserService} from "../../../_services/user.service";
import {User} from "../../../_models/user";
import {MatDialogRef} from "@angular/material/dialog";
import {FileDTO} from "../../../_models/file";
import {ApiResponseEntity} from "../../_interfaces/apiResponse";
import ApiResponse = ApiResponseEntity.ApiResponse;
import {HttpEvent, HttpEventType} from "@angular/common/http";
import {map} from "rxjs/operators";
import {NotificationService} from "../../../_services/notification.service";

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  selectedImageFile: File;
  post: any;
  user: User;
  postImage: FileDTO;
  upload = false
  progress: number = 0;

  /*public get userValue(): User {
    return this.userSubject.value;
  }


  this.userSubject = new BehaviorSubject<User>(null);
  this.user = this.userSubject.asObservable();
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;*/

  constructor(
    private postService: PostService,
    private userService: UserService,
    private notificationService: NotificationService,
    private dialog: MatDialogRef<CreatePostComponent>
  ) { }

  ngOnInit(): void {
  }

  onPhotoSelected(photoSelector: HTMLInputElement) {
    this.selectedImageFile = photoSelector.files[0];
    if(!this.selectedImageFile) return;
    let fileReader = new FileReader();
    fileReader.readAsDataURL(this.selectedImageFile);
    fileReader.addEventListener(
      "loadend",
      ev => {
        let readableString = fileReader.result.toString();
        const postDTO : FileDTO = new FileDTO({
          base64: fileReader.result.toString().split(',')[1] ,
          name: this.selectedImageFile.name,
          size: this.selectedImageFile.size,
          lastModified: this.selectedImageFile.lastModified,
          contentType: this.selectedImageFile.type}
        );
        this.postImage = postDTO
        document.getElementById("post-preview-image");
        let postPreviewImage = <HTMLImageElement>document.getElementById("post-preview-image");
        postPreviewImage.src = readableString;
      }
    )
  }

  onPostClick(postBody: HTMLTextAreaElement) {
    let postText = postBody.value;
    const user : User = this.userService.getUserDetails()
    console.log(user)
    if (postText.length <= 0 ) return;
    if(this.selectedImageFile){
      this.post     = new Post({
        body: postText,
        dateCreated: new Date().toISOString().split('.')[0].replace('T', '@'),
        image: this.postImage,
        username: user.userName
      })
    }else{
      this.post     = new Post({
        body: postText,
        dateCreated: new Date().toISOString().split('.')[0].replace('T', '@'),
        image: this.postImage,
        username: user.userName
      })
    }
    this.userService.submitPost(this.post)
      .pipe(map((apiResponse : ApiResponse) => {
        this.post = apiResponse.data
    }))
      .subscribe(next => {
        this.notificationService.showSuccess("Your post has been uploaded!")
        })
      .add(this.dialog.close())

  }

  closeDialog() {
    this.dialog.close()
  }
}
