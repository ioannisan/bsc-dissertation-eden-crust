import {FileDTO} from "../../_models/file";
import {Topic} from "../../_models/topic";
import {Post} from "../../_models/post";

export module ApiResponseEntity {

  export interface Topic {
    id: number;
    title: string;
  }

  export interface Avatar {
    id: number;
    name: string;
    contentType: string;
    size: number;
    data: string;
  }

  export interface Friendship {
    id: number;
  }

  export interface User {
    id: number;
    userName: string;
    user: string | null;
    password?: string | null;
    firstName: string;
    lastName: string;
    accessToken: string;
    refreshToken: string;
    accessTokenExpiration: number;
    refreshTokenExpiration: number;
    dateOfBirth?: string | null;
    files?: File [] | null;
    avatar?: FileDTO | null | any;
    email?: string | null;
    about?: string | null;
    gender?: string | null;
    topics?: Topic [] | null;
    posts?: Post [] | null;
  }

  export interface Image {
    id: number;
    name: string;
    contentType: string;
    data: string;
  }

  export interface Comment {
    id: number;
    dateCreated: string;
    body: string;
    user: string;
  }

  export interface New {
    abstract: string,
    web_url: string,
    snippet: string,
    lead_paragraph: string,
    source: string,
    topic: string,
    multimedia: Multimedia[],
    pub_date: string
  }

  export interface Multimedia {
    url: string,
    width: number,
    height: number
  }

  export interface Datum {
    id: number;
    firstName?: string;
    lastName?: string;
    avatar?: Avatar;
    userName?: string;
    dateCreated?: string;
    body?: string;
    likes?: number;
    topics?: Topic[];
    image?: Image;
    comments?: Comment[];
    user?: string;
  }

  export interface ApiResponse {
    transactionId: string;
    createdAt: string;
    data: Datum[];
  }

}
