import {Component, OnInit} from '@angular/core';
import { MatBottomSheet } from "@angular/material/bottom-sheet";
import {AuthenticationService} from "../../_services/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css']
})
export class WelcomePageComponent implements OnInit {


  state = ComponentTransitionState.INFO_COMPONENT;

  onRegisterClick(){
    this.state = ComponentTransitionState.REGISTER_COMPONENT;
  }

  onLoginClick(){
    this.state = ComponentTransitionState.LOGIN_COMPONENT;
  }

  onInfoClick(){
    this.state = ComponentTransitionState.INFO_COMPONENT;
  }

  isLoginState(){
    return this.state == ComponentTransitionState.LOGIN_COMPONENT;
  }

  isRegisterState(){
    return this.state == ComponentTransitionState.REGISTER_COMPONENT;
  }

  isInfoState(){
    return this.state == ComponentTransitionState.INFO_COMPONENT;
  }

  constructor(
    private loginSheet: MatBottomSheet,
    private authenticationService : AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    if (this.authenticationService.userValue) {
      this.router.navigate(['/home']);
    }else{
      console.log("User not authenticated");
    }
  }

  ngOnInit(): void {
  }
}

export enum ComponentTransitionState {
  LOGIN_COMPONENT,
  REGISTER_COMPONENT,
  INFO_COMPONENT
}

