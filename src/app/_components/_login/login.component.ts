import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthenticationService } from "../../_services/authentication.service";
import { first } from "rxjs";
import {Store} from "@ngrx/store";
import * as AuthActions from '../../_state/authentication/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loading = false;
  submitted = false;
  returnUrl: string | any;
  loginForm: FormGroup;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private store: Store,
  ) {
    /*if (this.authenticationService.userValue) {
      this.router.navigate(['/home']);
    }*/
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    /*this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';*/
  }

  get credentials() { return this.loginForm.controls; }
  /*onResetClick(resetEmail: HTMLInputElement){
    let email = resetEmail.value;
    if(this.isNotEmpty(email)){

    }
  }*/


  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    const credentials = {
      username: this.credentials['username'].value,
      password: this.credentials['password'].value
    }
    this.store.dispatch(AuthActions.loginRequest({credentials}));
    /*this.authenticationService.login(this.credentials['username'].value, this.credentials['password'].value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate([this.returnUrl]);
        },
        error: error => {
          this.error = error;
          this.loading = false;
        }
      });*/
  }

}


export enum AuthenticatorCompState {
  LOGIN,
  REGISTER,
  FORGOT_PASSWORD

}
