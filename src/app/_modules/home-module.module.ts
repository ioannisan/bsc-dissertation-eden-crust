import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreatePostComponent } from '../_components/_home/_posts/create-post.component';

//import { NavbarComponent } from '../_components/_navbar/navbar.component';
import { MaterialModule } from "./material.module";
import { SharedModuleModule } from "./shared-module.module";
import { WelcomePageComponent } from '../_components/_index/welcome-page.component';
import { FooterComponent } from '../_components/_footer/footer.component';
import { RegistrationStepperComponent } from '../_components/_registration/registration-stepper.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import { HomeComponent } from '../_components/home/home.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { PostComponent } from '../_components/_posts/post.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { FriendsComponent } from '../_components/_friends/friends.component';
//import { TrendingComponent } from '../_components/_trending/trending.component';
import { RecommendedFriendsComponent } from '../_components/_friends/recommended-friends.component';
import { UserSettingsComponent } from '../_components/settings/user-settings.component';
import { NewComponent } from '../_components/_news/new.component';
import { LandingSectionComponent } from '../_components/_index/landing-section.component';
/*import { CreatePostComponent } from "../_components/_posts/create-post.component";*/



@NgModule({
  /*providers: [
    PostComponent
  ],*/
  declarations: [
    CreatePostComponent,
    //NavbarComponent,
    WelcomePageComponent,
    FooterComponent,
    RegistrationStepperComponent,
    HomeComponent,
    //TrendingComponent,
    RecommendedFriendsComponent,
    UserSettingsComponent,
    NewComponent,
    LandingSectionComponent
  ],
  exports: [
    //NavbarComponent,
    MaterialModule,
    SharedModuleModule,
    WelcomePageComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModuleModule,
    RouterModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule
  ]
})
export class HomeModuleModule { }
