import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../_components/_login/login.component';
import { MaterialModule } from "./material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {ErrorHandlingService} from "../_services/error-handling.service";
import { AlertDialogComponent } from '../_error_handlers/alert-dialog.component';
import { NavComponent } from '../_components/_navbar/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {RouterModule} from "@angular/router";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatDividerModule} from "@angular/material/divider";
import {MatCardModule} from "@angular/material/card";
import {FlexLayoutModule} from '@angular/flex-layout';
import { ErrorHandlerComponent } from '../_error_handlers/error-handler.component';
import {NotificationMenuComponent} from "../_components/_notifications/notification-menu.component";
import {ClickOutsideModule} from "ng-click-outside";
import {PostComponent} from "../_components/_posts/post.component";



@NgModule({
  declarations: [
    LoginComponent,
    AlertDialogComponent,
    NavComponent,
    ErrorHandlerComponent,
    NotificationMenuComponent,
    PostComponent
  ],
  imports: [
    MatCardModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    RouterModule,
    FlexLayoutModule,
    ClickOutsideModule
  ],
  exports: [
    CommonModule,
    LoginComponent,
    MaterialModule,
    AlertDialogComponent,
    NavComponent,
    FlexLayoutModule,
    NotificationMenuComponent,
    PostComponent
  ],
  providers: [
    ErrorHandlingService,
    ErrorHandlerComponent
  ]
})
export class SharedModuleModule { }
