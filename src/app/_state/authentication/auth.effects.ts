import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import * as AuthActions from '../authentication/auth.actions';
import {catchError, exhaustMap, of, map, tap, first, mergeMap} from "rxjs";
import {AuthenticationService} from "../../_services/authentication.service";
import {Router} from "@angular/router";
import {loginFailure, loginRequest, loginSuccess, registrationSuccess} from "../authentication/auth.actions";
import {User} from "../../_models/user";
import {UserService} from "../../_services/user.service";
import {ErrorHandlerComponent} from "../../_error_handlers/error-handler.component";

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private router: Router,
    private errorDisplay: ErrorHandlerComponent
  ) {}


  loginRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loginRequest),
      exhaustMap((action) => {
        return this.authenticationService.login(action.credentials.username, action.credentials.password).pipe(
          first(),
          mergeMap((user : User) => {
            //this.userService.getUserTopics();
            const user_profile = this.authenticationService.userValue;
            this.authenticationService.saveUserToLocalStorage(user)
            return of(loginSuccess({user}))
          }),
          catchError(error => {
            return of(loginFailure({message: error.error}));
          })
        )
      })
    )
  })

  loginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      tap(user   => {
          console.log(user)
          this.router.navigateByUrl('/home');
        })
      ),
      {dispatch: false}
  );

  loginFailure$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginFailure),
      tap(message => {
        this.errorDisplay.openErrorMessage(message.message, "Close")
        console.log(message);
      })
    ),
    {dispatch: false}
  );

  registrationRequest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.registrationRequest),
      exhaustMap((action) => {
        return this.authenticationService.register(action.userDetails).pipe(
          first(),
          mergeMap((user : User) => {
            this.authenticationService.saveUserToLocalStorage(user)
            return of(registrationSuccess({user}))
          })
        )
      })
    )
  })

  registrationSuccess$ = createEffect( () =>
    this.actions$.pipe(
      ofType(AuthActions.registrationSuccess),
      tap(user => {
        this.router.navigateByUrl('/home');
      })
    ),
    {dispatch: false}
  );

  registrationFailure$ = createEffect( () =>
    this.actions$.pipe(
      ofType(AuthActions.registrationFailure),
      tap(message => {
        console.log(message);
      })
    )
  );

}
