import {Actions, createEffect, ofType} from "@ngrx/effects";
import * as PostActions from "../posts/posts.actions";
import {exhaustMap, first, mergeMap, of, tap} from "rxjs";
import {User} from "../../_models/user";
import {registrationSuccess} from "../authentication/auth.actions";
import {relatedTopicsFailure, relatedTopicsRequest, relatedTopicsSuccess} from "./posts.actions";
import {PostService} from "../../_services/post.service";
import {UserService} from "../../_services/user.service";
import {catchError, map} from "rxjs/operators";
import {Post} from "../../_models/post";
import {ErrorHandlerComponent} from "../../_error_handlers/error-handler.component";

export class PostsEffects {


    constructor(
      private actions$: Actions,
      private postService: PostService,
      private userService: UserService,
      private errorDisplay: ErrorHandlerComponent
    ) {
    }

    //Bring State management to post fetching
    relatedTopicsRequest$ = createEffect(() => {
      return this.actions$.pipe(
        ofType(PostActions.relatedTopicsRequest),
        exhaustMap((action) => {
          return this.userService.getTopicRelatedPosts(action.options.limit)
            .pipe(
              map((posts : Post[]) => {
                return relatedTopicsSuccess({posts: posts})
              }),
              catchError(error => {
                return of(relatedTopicsFailure({message: "There was an error while fetching the posts, please try again later"}))
              })
          )
        })
      )
    })

    relatedTopicsSuccess$ = createEffect( () =>
        this.actions$.pipe(
          ofType(PostActions.relatedTopicsSuccess),
          tap(user => {
          })
        ),
      {dispatch: false}
    );

    relatedTopicsFailure$ = createEffect( () =>
      this.actions$.pipe(
        ofType(PostActions.relatedTopicsFailure),
        tap(message => {
          console.log(message);
          this.errorDisplay.openErrorMessage(message.message, "Close")
          console.log(message);
        })
      )
    );

}
