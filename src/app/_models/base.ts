export class ResponseEntity<T = any> {
  transactionId: string;
  createdAt: string;
  data: T | T[];
  apiError: string;

  constructor(res_data?: any) {
    if(res_data){
      this.transactionId = res_data.transactionId;
      this.createdAt = res_data.createdAt;
      this.data = res_data.data;
      this.apiError = res_data.apiError;
    }
  }

}
