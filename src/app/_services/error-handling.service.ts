import {ErrorHandler, Injectable, NgZone} from "@angular/core";
import {HttpErrorResponse} from "@angular/common/http";

const KNOWN_ERRORS = [400, 401, 403, 409];


@Injectable()
export class ErrorHandlingService extends ErrorHandler{

  getClientErrorMessage(error: Error): string {
    return error.message ?
      error.message :
      error.toString();
  }

  getServerErrorMessage(error: HttpErrorResponse): string {
    return error.status==404 ?
      "This resource is not currently available, please try again later!" :
      'No Internet Connection';
  }
}


/*** @description it returns true for all errors,
 * known in the app, so that no redirect to error-page takes place
 * @param errorCode — error status code
 */
export function isKnownError(errorCode: number): boolean {
  return KNOWN_ERRORS.includes(errorCode);
}
