﻿import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';


import {BehaviorSubject, last, Observable} from 'rxjs';
import {AuthenticationService} from "./authentication.service";
import {User} from "../_models/user";
import {environment} from "../../environments/environment";
import {Post} from "../_models/post";
import {FormArray, FormControl} from "@angular/forms";
import {FileDTO} from "../_models/file";
import {Topic} from "../_models/topic";
import {assertCompatibleAngularVersion} from "@angular-devkit/build-angular/src/utils/version";
import {map} from "rxjs/operators";
import {IPost} from "../_models/IPost";
import {ResponseEntity} from "../_models/base";
import {ApiResponseEntity} from "../_components/_interfaces/apiResponse";
import ApiResponse = ApiResponseEntity.ApiResponse;
import {NotificationService} from "./notification.service";

interface FileDTOI {
  _base64?: string;
  _name?: string;
  _size?: number;
  _lastModified?: number;
  _contentType?: string;
}


@Injectable({ providedIn: 'root' })
export class UserService {

  avatar : FileDTO;

  constructor(
    private http: HttpClient,
    private notificationService: NotificationService
  ) {
    this.userSubject = new BehaviorSubject<User>(null);
    this.user = this.userSubject.asObservable();
  }

  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  public get userValue(): User {
    return this.userSubject.value;
  }

  getRecommendedFriends(userId: number) : Observable<ApiResponse> {
    const options = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'action': 'getAllRecommendedFriends'
      }),
      withCredentials: true
    };

    return this.http.get<ApiResponse>(`${environment.apiUrl}/users/recommend/${userId}`, options)
  }

  getUserProfile(username: string) : Observable<User> {
    const options = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'action' : 'findUserProfile'
      }),
      withCredentials: true
    };

    return this.http.get<User>(`${environment.apiUrl}/users/find/${username}`, options)
      .pipe(map((responseData : ResponseEntity) => {
          this.userSubject.next(responseData.data);
          return responseData.data
        }
      ))
  }


  updateUserProfile(username : string, email : string, topics : string[]){
    const updateProfile = {
      'username' : this.getUserDetails().userName,
      'newUsername': username,
      'newEmail' : email,
      'newTopics' : topics
    }
    const options = {
      headers: new HttpHeaders({
        Action: 'updateUserSettings',
        'Content-Type': 'application/json',
      }),
      withCredentials: true
    }
    return this.http.patch<ApiResponse>(`${environment.apiUrl}/users/`, updateProfile, options)
      .pipe(next => {
        this.updateUserDetails(username, email)
        this.notificationService.showSuccess("Your settings have been updated!")
        return next
      })
  }

  findUser(id: string) {
    return this.http.get<User>(`${environment.apiUrl}/users/${id}`, {withCredentials:true})
  }

  getAll() {
    return this.http.get<User[]>(`${environment.apiUrl}/users`, {withCredentials:true});
  }

  getTopicRelatedPosts(limit: number, page: number) : Observable<ApiResponse> {
    const username = this.getUserDetails().userName;
    const options = {
      headers: new HttpHeaders({
        Action: 'findTopicRelated',
        'Content-Type': 'application/json',
      }),
      params: new HttpParams()
        .append('username', username)
        .append('limit', limit.toString())
        .append('page', page.toString()),
      withCredentials: true
    }
    return this.http.get<ApiResponse>(`${environment.apiUrl}/users`, options);
  }

  getUserTopics() : Observable<ApiResponse>{
    const username = this.getUserDetails().userName;
    const options = {
      headers: new HttpHeaders({
        Action: 'findByUsers',
        'Content-Type': 'application/json'
      }),
      params: new HttpParams()
        .append("users", username),
      withCredentials: true
    }
    return this.http.get<ApiResponse>(`${environment.apiUrl}/topics/find`, options)
  }

  getFriendPosts(username: string, limit: number, page: number) : Observable<ApiResponse> {
    const options = {
      headers: new HttpHeaders({
        Action: 'findFriendsPosts',
        'Content-Type': 'application/json',
        'Content-Length': (username.length).toString()
      }),
      params: new HttpParams()
        .append('limit', limit.toString())
        .append('page', page.toString()),
      withCredentials: true
    };

    return this.http.get<ApiResponse>(`${environment.apiUrl}/users/${username}`, options)
  }

  getPosts(username: string, limit: number, page: number) :  Observable<ApiResponse> {
    const options = {
      headers: new HttpHeaders({
        Action: 'findPostsByUsername',
        'Content-Type': 'application/json',
        'Content-Length': (username.length).toString()
      }),
      params: new HttpParams()
        .append('limit', limit.toString())
        .append('page', page.toString()),
      withCredentials: true
    };
    return this.http.get<ApiResponse>(`${environment.apiUrl}/users/${username}`, options)
  }

  submitPost(post: Post) : Observable<ApiResponse>{
    return this.http.post<ApiResponse>(`${environment.apiUrl}/users/upload`, post, {
      withCredentials: true
    })
  }

  getRelatedPosts(limit: number, page: number) :  Observable<ApiResponse> {
    const user = this.getUserDetails()
    console.warn(user)
    const username = this.getUserDetails().userName;
    const options = {
      headers: new HttpHeaders({
        Action: 'findUserInterestPosts',
        'Content-Type': 'application/json',
        'Content-Length': (username.length).toString()
      }),
      params: new HttpParams()
        .append('username', username)
        .append('limit', limit.toString())
        .append('page', page.toString()),
      withCredentials: true
    };
    return this.http.get<ApiResponse>(`${environment.apiUrl}/users`, options)
  }

  updateUserDetails(username, email) {
    const user : User = this.getUserDetails()
    user.userName = (username!=null) ? username : user.userName
    user.email = (email != null) ? email : user.email
    console.warn(user)
    localStorage.setItem('user-profile', JSON.stringify(user))
  }

  getUserDetails(): User {
    return JSON.parse(localStorage.getItem('user-profile'));
  }

  filterDate(dateField: FormControl): string {

    return '';
  }
}
